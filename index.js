const express = require("express");

// Mongoose is a package that allows creation of schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose")

const app = express();
const port = 3001;

// MongoDB connection
// Conect to the database by passing in our connection string, remember to replace the password and database names with actual values
// {newUrlParser:true} allows us to avoid any current and future errors while connecting to MongoDB

//Syntax
	//mongoose.connect("<MongoDB connection string>", {useNewUrlParser:true})

mongoose.connect("mongodb+srv://admin1234:admin123@project-0.h4crfak.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
	}

);

//Set notifications for connection success or failure
//Connection to the database
//Allows us to handle errors when the initial connection is estbalished
//Works with the on and once Mongoose methods

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
//If a connection error occured, output in the console
//console.error.bind(console) allows us to print errors in our terminal

db.once("open", () => console.log("We're connected to the cloud database"))

// Mongoose Schemas
// Schemas determine the structure of the documents to be written in the database
// Use the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema

const taskSchema = new mongoose.Schema({

	//define the fields with their corresponding data type

	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

const userSchema = new mongoose.Schema({

	//define the fields with their corresponding data type

	username: String,
	password: String,
	status: {
		type: String,
		default: "pending"
	}
})

// Models
// Uses Schemas and are used to create/instantiate objects that correspond to the schema
// Server > Schema(blueprint) > Database > Collection
// models must be in singular form and Capitalized

const Task = mongoose.model("Task", taskSchema);

const User = mongoose.model("User", userSchema);


//Setup for allowing the server to handle data from requests
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Create a new task
// Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		- if the task already exists in the database, we return an error
		- if the task does not exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/


app.post("/tasks" , (req, res) => {

	Task.findOne({name: req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found!")
		}
		else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr, savedTask) => {
				if (saveErr){
					return console.error(saveErr);
				}
				else {
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

app.post("/signup" , (req, res) => {

	User.findOne({username: req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate user found!")
		}
		else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((saveErr, savedUser) => {
				if (saveErr){
					return console.error(saveErr);
				}
				else {
					return res.status(201).send("New user created");
				}
			})
		}
	})
})

//Getting all the Tasks
//Business Logic
/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents

*/

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data: result
			})
		}
	})
})


app.get("/signup", (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data: result
			})
		}
	})
})
//Activity

// 1. Create a user schema
// 2. Create a user model
// 3. Create a POST route that will access the /signup route that will create a user

// Registering a user
// Business logic

/*
	1. Add a functionality to check if there are duplicate tasks
		- If the user already exists in the database, we return an error
		- If the user doesn't exist in the database, we add it in the database

	2. The user data will be coming from the request's body
	3. Create a new User object with a "username" and "password" fields/properties
*/


// 4. Process a POST request at the /signup route using postman to register a user. 
// 5. Create a git repository named s35.
// 6. Initialize a local git repository, add the remote link and push to git with the commit message of "Add s35 activity code"
// 7. Add the ink in Boodle.
// 8. Send a screenshot of the /signup route in Postman to our Batch Hangouts

// Stretch Goal - Create a route that will retrieve all users.


app.listen(port, () => console.log(`Server running at port ${port}.`));
